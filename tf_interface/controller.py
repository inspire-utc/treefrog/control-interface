import websockets 
import asyncio 
import sys 
import json 
import collections
def log(*args, **kwargs):
    print(*args, **kwargs, file=sys.stderr)


class Controller:


    def __init__(self, port, host="localhost"):
        self.port = port
        self.host = host 
        self.clients = set()
        self.loop = asyncio.get_event_loop()

    async def _handler(self, ws, path):
        log("Client Connected")
        self.clients.add(ws)
        try:
            async for message in ws:
                data = collections.defaultdict(lambda: None, json.loads(message))
                command = data["command"]
                action = data["action"]
                params = data["params"]
                log(f"{command} - {action}")
                if command == "x-arm-left": 
                    self.loop.create_task(self.handle_x_arm_left(params))

        finally:
            self.clients.remove(ws)
            log("Client Disconnected")

    async def serve(self):        
        print(self.host, self.port)
        async with websockets.serve(self._handler, self.host, self.port):
            await asyncio.Future()

    def handle_x_arm_left(self):
        pass
