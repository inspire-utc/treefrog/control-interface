"USAGE: treefrog-ui <port>"

try:
    from tf_interface import controller 
except: 
    import controller 

import os 
import pty
import docopt 
import asyncio 
import time
class TreeFrog(controller.Controller):
   
    def __init__(self, *args, fd=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.fd = fd
   
    async def handle_x_arm_left(self, params=None):
        os.write(self.fd, bytes("echo hello", encoding="utf-8"))
        print(os.read(self.fd, 100))
def main(): 
    args = docopt.docopt(__doc__)
    port = args["<port>"]
    loop = asyncio.get_event_loop()
    
    pid, fd = pty.fork()
    if pid == 0: # Child 
        import subprocess 
        child = subprocess.call(["bash"])
    else:

        treefrog_controller = TreeFrog(port, fd=fd)
        loop.create_task(treefrog_controller.serve())
        loop.run_forever()

if __name__ == "__main__":
    main()

