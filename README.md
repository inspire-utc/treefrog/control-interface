# Control Interface

This repo contains the code for TreeFrog's control interface

## Front End
The front end consists of a single page webapp. Core to its functionality as a real-time controller is a websocket client connection

## Back End
The back end consists of an async websocket server that interfaces directly with ROS
