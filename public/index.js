
let socket = new WebSocket(document.WebsocketURL)

const BUTTONS = [
    "x-arm-left",
    "x-arm-right",
    "x-arm-lift",
    "x-arm-lower",
    
    "y-arm-up",
    "y-arm-down",
    "y-arm-lift",
    "y-arm-lower",

    "sonar-lift",
    "sonar-lower",
]

function initActionButton(button, command)
{
    let message_start = JSON.stringify({
        command,
        action: "start"
    })

    let message_stop = JSON.stringify({
        command,
        action: "stop"
    })

    button.addEventListener('mousedown', () => socket.send(message_start))
    button.addEventListener('mouseup', () => socket.send(message_stop))
}

BUTTONS.forEach((id) => 
    {
        let btn = document.getElementById(id)
        initActionButton(btn, id)
    } )
